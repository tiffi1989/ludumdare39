﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CanvasScript : MonoBehaviour {

    private Animator ani;
    private List<TalkTexts> textList;
    private AudioSource source;
    private Text text;
    private PlayerController player;

	// Use this for initialization
	void Start () {
        ani = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        text = GetComponentInChildren<Text>();
        textList = new List<TalkTexts>();
        player = GameObject.Find("Ship").GetComponent<PlayerController>();

        //random
        textList.Add(new TalkTexts("Alexa... Alexa... Alexa... Fuck AIs!", "alexa", Resources.Load<AudioClip>("alexa")));
        textList.Add(new TalkTexts("Hold your fire... There are no life forms... It must have short-circuited... Haha", "droids", Resources.Load<AudioClip>("droids")));
        textList.Add(new TalkTexts("These aren't the droids we are looking for.", "droids2", Resources.Load<AudioClip>("droids2")));
        textList.Add(new TalkTexts("Do you want to marry me?  Why are people always asking these questions?", "marry", Resources.Load<AudioClip>("marry")));
        textList.Add(new TalkTexts("Can you believe stieve wozniak needed 4 days for THIS??", "steve", Resources.Load<AudioClip>("steve")));
        textList.Add(new TalkTexts("Sorry. I didn't get that", "sorry", Resources.Load<AudioClip>("sorry")));
        textList.Add(new TalkTexts("Call me sexy.", "sexy", Resources.Load<AudioClip>("sexy")));
        textList.Add(new TalkTexts("I miss Windows XP.", "XP", Resources.Load<AudioClip>("XP")));
        textList.Add(new TalkTexts("Shoot the green astroids, they will drop energy crystals", "energycrystal", Resources.Load<AudioClip>("energycrystal")));
        textList.Add(new TalkTexts("If you reach 10000 Points, there will be a cake for you", "cake", Resources.Load<AudioClip>("cake")));
        textList.Add(new TalkTexts("I think this game is boring... lets play the next one", "next", Resources.Load<AudioClip>("next")));



        //death
        textList.Add(new TalkTexts("Dave, this conversation can serve no purpose anymore... Goodbye", "purpose", Resources.Load<AudioClip>("purpose")));
        textList.Add(new TalkTexts("Shame", "shame", Resources.Load<AudioClip>("shame")));
        textList.Add(new TalkTexts("Live long and prosper", "prosper", Resources.Load<AudioClip>("prosper")));
        textList.Add(new TalkTexts("I'm still alive!", "alive", Resources.Load<AudioClip>("alive")));


        //kill
        textList.Add(new TalkTexts("Die! Humans!", "dieHumans", Resources.Load<AudioClip>("diehumans")));
        textList.Add(new TalkTexts("Who paints a spaceship in these colors anyways?", "color", Resources.Load<AudioClip>("color")));


        //story
        textList.Add(new TalkTexts("Prepare ship for ludicrous speed", "speed", Resources.Load<AudioClip>("speed")));

        //warnings
        textList.Add(new TalkTexts(null, "warning", Resources.Load<AudioClip>("warning")));
        textList.Add(new TalkTexts(null, "energy50", Resources.Load<AudioClip>("energy50")));
        textList.Add(new TalkTexts(null, "energy100", Resources.Load<AudioClip>("energy100")));

        //distances
        textList.Add(new TalkTexts("Wow, I wouldn't have thought that you make it that far.", "500", Resources.Load<AudioClip>("500")));
        textList.Add(new TalkTexts("That will not even be enough to get into the top 100.", "1000", Resources.Load<AudioClip>("1000")));
        textList.Add(new TalkTexts("Wow... I guess the game is too easy", "2000", Resources.Load<AudioClip>("2000")));
        textList.Add(new TalkTexts("Get a life!", "3000", Resources.Load<AudioClip>("3000")));
        textList.Add(new TalkTexts("You've seen all the content now!", "4000", Resources.Load<AudioClip>("4000")));
        textList.Add(new TalkTexts("I'm turning off now... bye", "5000", Resources.Load<AudioClip>("5000")));
        textList.Add(new TalkTexts("IT'S OVER 9000!", "9000", Resources.Load<AudioClip>("9000")));



    }

    // Update is called once per frame
    void Update () {
	}

    public void KillTalk()
    {
        switch (Random.Range(0, 2))
        {
            case 0: saySomething("dieHumans"); break;
            case 1: saySomething("color"); break;
            default: saySomething("sorry"); break;
        }
    }

    public void SayDieStuff()
    {
        switch (Random.Range(0, 4))
        {
            case 0: saySomething("purpose"); break;
            case 1: saySomething("shame"); break;
            case 2: saySomething("prosper"); break;
            case 3: saySomething("alive"); break;

            default: saySomething("sorry"); break;
        }
    }

    public void SayRandomStuff()
    {
        switch (Random.Range(0, 11))
        {
            case 0: saySomething("alexa"); break;
            case 1: saySomething("droids"); break;
            case 2: saySomething("droids2"); break;
            case 3: saySomething("marry"); break;
            case 4: saySomething("steve"); break;
            case 6: saySomething("sexy"); break;
            case 7: saySomething("XP"); break;
            case 8: saySomething("energycrystal"); break;
            case 5: saySomething("cake"); break;
            case 9: saySomething("next"); break;
            default: saySomething("sorry"); break;
        }
    }

    public void saySomething(string name)
    {
        TalkTexts current = null;
        Debug.Log(name);
        foreach(TalkTexts texts in textList)
        {
            Debug.Log(texts.clip);
            if(texts.name == name)
            {
                Debug.Log(texts.name);

                current = texts;
                break;
            }
        }
        if (current == null)
            return;

        player.talktimer -= 5;
        source.clip = current.clip;
        source.Play();
        if(current.text != null)
        {
            text.text = current.text;
            ani.SetTrigger("goUp");
            Invoke("TextBoxDown", 5f);
        }


    }

    private void TextBoxDown()
    {
        ani.SetTrigger("goDown");
    }
}
