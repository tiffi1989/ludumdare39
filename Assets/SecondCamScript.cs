﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondCamScript : MonoBehaviour {

    private GameObject player;

    // Use this for initialization
    void Start()
    {
        this.player = GameObject.Find("Ship");
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
        transform.rotation = player.transform.rotation;
    }
}
