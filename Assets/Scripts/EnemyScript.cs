﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    public GameObject bulletPrefab;
    public float shotspeed;
    private PlayerController player;
    private GameObject playerObject;
    public Transform gunTransform;
    public int life;
    public float shootFrequency;
    private float shootTimer;
    private Rigidbody2D rig;
    private bool dead;
    public GameObject explosionPrefab;
    public float maxSpeed, speed, rotationSpeed;
    private CanvasScript talk;
    public GameObject bonusDrop;

    // Use this for initialization
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        playerObject = GameObject.Find("Ship");
        player = playerObject.GetComponent<PlayerController>();
        talk = GameObject.Find("TextBox").GetComponent<CanvasScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!player.isGameStarted() || dead)
        {
            return;
        }

        if(life <= 0)
        {
            die();
        }

        float distance2Player = Vector3.Distance(playerObject.transform.position, transform.position);

        if (distance2Player < 7)
        {
            rig.drag = .95f;
        }
        else if (distance2Player < 30)
        {
            rig.drag = 0;
            if (rig.velocity.magnitude <= maxSpeed)
                rig.AddForce((playerObject.transform.position - transform.position).normalized * speed * Time.deltaTime);
            float angleLook = (Mathf.Atan2(playerObject.transform.position.y - transform.position.y, playerObject.transform.position.x - transform.position.x) * Mathf.Rad2Deg);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, angleLook), rotationSpeed * Time.deltaTime);
        }
        else {
            rig.velocity = new Vector2(0, 0);
        }



        shootTimer += Time.deltaTime;

        Vector3 target = playerObject.transform.position;
        Vector3 screenPoint = gunTransform.position;
        Vector2 offset = new Vector2(target.x - screenPoint.x, target.y - screenPoint.y);
        float angle = Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg;
        gunTransform.rotation = Quaternion.Euler(0, 0, angle);

        if (shootTimer > shootFrequency - Random.Range(-1,1f))
        {
            GameObject bullet = Instantiate(bulletPrefab, gunTransform.position, gunTransform.rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = Turning().normalized * shotspeed;
            bullet.tag = "enemyShot";
            shootTimer = 0;
        }

    }

    void die()
    {
        dead = true;

        if(Random.Range(0f,5f) < 1f)
        {
            talk.KillTalk();
        }

        if (Random.Range(0,10) == 5)
        {
            Instantiate(bonusDrop, transform.position, Quaternion.identity).GetComponent<Rigidbody2D>().AddTorque(1f);
        }



        GetComponent<PolygonCollider2D>().enabled = false;
        rig.velocity = new Vector2();
        rig.angularVelocity = 0;
        SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
        foreach(SpriteRenderer render in renderers)
        {
            render.enabled = false;
        }
        GameObject explosion = Instantiate(explosionPrefab, new Vector3(transform.position.x, transform.position.y, -2f), Quaternion.Euler(-90, 0, 0));
        Invoke("destroyGO", .5f);
    }

    private void destroyGO()
    {
        Destroy(gameObject);
    }

    Vector2 Turning()
    {
        Vector3 shotPixelposition = gunTransform.position;
        return new Vector2(
            playerObject.transform.position.x - shotPixelposition.x,
            playerObject.transform.position.y - shotPixelposition.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Shot")
        {
            life--;
            Destroy(collision.gameObject);
        }
    }
}
