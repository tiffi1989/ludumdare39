﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour {

    private Rigidbody2D rig;
    public float speed, rotationSpeed, breakSpeed;
    private float angularDragStart, dragStart;
    private Camera cam;
    public GameObject explosion;
    private bool dead, inWarp, gameStarted;
    public Animator deflectorAni;
    public PowerBarScript powerBar;
    public float maxSpeed;
    public GameObject[] throttlers;
    public SpriteRenderer boost;
    public Highscores highscores;
    public Animator canvasAni;
    private string name;
    public InputField nameInput;
    public DistanceTracker distance;
    private CanvasScript talk;
    public float talktimer;
    private AudioSource source;


    // Use this for initialization
    void Start () {
        this.rig = GetComponent<Rigidbody2D>();
        this.cam = Camera.main;
        this.angularDragStart = rig.angularDrag;
        this.dragStart = rig.drag;
        talk = GameObject.Find("TextBox").GetComponent<CanvasScript>();
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
   


        if (!gameStarted)
            return;

        if(!dead && !inWarp)
        {
            movement();
            talktimer += Time.deltaTime;
            if (talktimer > 30f)
            {
                talk.SayRandomStuff();
                talktimer = 0;
            }
        }




    }

    public void die()
    {
        rig.velocity = new Vector2();
        SpriteRenderer[] renderes = GetComponentsInChildren<SpriteRenderer>();
        foreach(SpriteRenderer rend in renderes)
        {
            rend.enabled = false;
        }
        if (!dead)
        {
            explode();
            Invoke("showHighscore", 2f);
            Invoke("explode", .3f);
            Invoke("explode", .6f);
            Invoke("explode", .9f);
            Invoke("explode", 1.2f);
            Invoke("explode", 1.5f);
            Invoke("explode", 1.8f);
            talk.SayDieStuff();

        }
        dead = true;

    }

    private void showHighscore()
    {
        canvasAni.SetTrigger("showHighscore");
        highscores.addHighscore(name, (int)distance.getFinalPoints());
    }

    void explode()
    {
        Instantiate(explosion, new Vector3(transform.position.x + Random.Range(-1.5f, 1.5f), transform.position.y + Random.Range(-1.5f, 1.5f), -2), Quaternion.identity);

    }

    void movement()
    {
        if (Input.GetButton("Fire2"))
        {

            if (powerBar.energy - 0.2f * Time.deltaTime <= 0)
                return;

            powerBar.modifyEnergy(-0.2f * Time.deltaTime);

            Vector3 playerPositionWorld = cam.WorldToScreenPoint(this.transform.position);
            Vector2 playerPostionPixel = new Vector2(playerPositionWorld.x, playerPositionWorld.y);
            Vector2 mousePosition = Input.mousePosition;
            if(rig.velocity.magnitude <= maxSpeed)
                rig.AddForce((mousePosition - playerPostionPixel).normalized * speed * Time.deltaTime);
            float angle = (Mathf.Atan2(mousePosition.y - playerPostionPixel.y, mousePosition.x - playerPostionPixel.x) * Mathf.Rad2Deg);
            rig.angularVelocity = 0;
            if(!source.isPlaying)
                source.Play();

            



            foreach (GameObject throttler in throttlers)
            {
                Vector3 direction3D = transform.position - throttler.transform.position;
                Vector2 directornThrot = new Vector2(direction3D.x, direction3D.y);
                if(Vector2.Dot(directornThrot.normalized, (mousePosition - playerPostionPixel).normalized) > .85f)
                {
                    throttler.GetComponent<SpriteRenderer>().enabled = true;
                    throttler.GetComponent<Light>().enabled = true;

                    if (throttler.tag == "tdummyleft")
                    {
                        foreach (GameObject throttlerInner in throttlers)
                        {
                            if(throttlerInner.tag == "tleft")
                            {
                                throttlerInner.GetComponent<SpriteRenderer>().enabled = true;
                                throttlerInner.GetComponent<Light>().enabled = true;
                            }
                        }
                    }
                    if (throttler.tag == "tdummyright")
                    {
                        foreach (GameObject throttlerInner in throttlers)
                        {
                            if (throttlerInner.tag == "tright")
                            {
                                throttlerInner.GetComponent<SpriteRenderer>().enabled = true;
                                throttlerInner.GetComponent<Light>().enabled = true;
                            }
                        }
                    }
                } else
                {
                    throttler.GetComponent<SpriteRenderer>().enabled = false;
                    throttler.GetComponent<Light>().enabled = false;
                }
            }

            /**if (mousePosition.y > playerPostionPixel.y)
                rig.AddTorque(1 * rotationSpeed * Time.deltaTime);
            else
                rig.AddTorque(-1 * rotationSpeed * Time.deltaTime);
    */
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, angle), rotationSpeed * Time.deltaTime);
        }

        if (Input.GetButton("Jump"))
        {
            rig.drag = breakSpeed;
            rig.angularDrag = breakSpeed;
            powerBar.modifyEnergy(-0.2f * Time.deltaTime);
            Debug.Log("BREAK");
            if (!source.isPlaying)
                source.Play();
            foreach (GameObject throttler in throttlers)
            {
                setAllThrottlers(true);
            }
        } else
        {
            rig.drag = dragStart;
            rig.angularDrag = angularDragStart;
        }

        if (!Input.GetButton("Jump") && !Input.GetButton("Fire2")) 
        {
            setAllThrottlers(false);
            source.Stop();
        }

        if (Input.GetButtonDown("Fire3") && powerBar.getWarpcores() > 0)
        {
            inWarp = true;
            powerBar.modifyWarpCores(-1);
            GetComponent<Animator>().SetTrigger("startWarp");
            rig.velocity = new Vector2();
            rig.angularVelocity = 0;
            rig.drag = dragStart;
            rig.angularDrag = angularDragStart;
            Invoke("startWarp", 3f);
            Invoke("endWarp", 10f);
        }
    }

    private void setAllThrottlers(bool value)
    {
        foreach (GameObject throttler in throttlers)
        {
            throttler.GetComponent<SpriteRenderer>().enabled = value;
            throttler.GetComponent<Light>().enabled = value;
        }
    }

    private void startWarp()
    {
        GetComponent<CircleCollider2D>().enabled = true;
        rig.AddForce((transform.rotation * Vector2.right).normalized * 3000) ;
        boost.enabled = true;
        Invoke("startBreaking", 4);
    }

    private void startBreaking()
    {
        rig.drag = breakSpeed;
    }

    private void endWarp()
    {
        rig.velocity = new Vector2();
        boost.enabled = false;
        GetComponent<Animator>().SetTrigger("endWarp");
        Invoke("reEnableMovement", 4f);
    }

    private void reEnableMovement()
    {
        inWarp = false;
        Debug.Log("MovementRe");
        GetComponent<CircleCollider2D>().enabled = false;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Asteroid")
        {
            deflectorAni.SetTrigger("hit");
            powerBar.modifyEnergy(-collision.gameObject.GetComponent<AsteroidScript>().getStartLife() < -15 ? -15 : -collision.gameObject.GetComponent<AsteroidScript>().getStartLife());
            collision.gameObject.GetComponent<AsteroidScript>().die();

        }
        
      
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemyShot")
        {
            deflectorAni.SetTrigger("hit");
            powerBar.modifyEnergy(-5);
            Destroy(collision.gameObject);
        }
    }

    public void StartGame()
    {
        Debug.Log(nameInput.text);
        if (nameInput.text != "")
        {
            gameStarted = true;
            canvasAni.SetTrigger("gameStarted");
            name = nameInput.text;
        }
        else
        {
            nameInput.transform.localScale = new Vector3(nameInput.transform.localScale.x * 1.2f, nameInput.transform.localScale.x * 1.2f, nameInput.transform.localScale.x * 1.2f);
        }
    }

    public bool isGameStarted()
    {
        return gameStarted;
    }

    public void restartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
