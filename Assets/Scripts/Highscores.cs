﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Highscores : MonoBehaviour {

    private string addScoreURL = "http://104.236.198.23/addhighscore.php?";
    private string highscoreURL = "http://104.236.198.23/gethighscore.php";
    public Text highscoreText;

    void Start()
    {

    }

    public void addHighscore(string name, int score)
    {
        StartCoroutine(PostScores(name, score));
    }

    public void quit()
    {
        Application.Quit();
    }

    public void getHighscore()
    {
        StartCoroutine(GetScores());
    }

    IEnumerator PostScores(string name, int score)
    {
        Debug.Log(name + " " + score);
        string post_url = addScoreURL + "name=" + WWW.EscapeURL(name) + "&score=" + score;
        highscoreText.text = "Loading Highscore";
        WWW hs_post = new WWW(post_url);
        yield return hs_post;
        StartCoroutine(GetScores());
    }

    IEnumerator GetScores()
    {
        WWW hs_get = new WWW(highscoreURL);

        highscoreText.text = "Loading Highscore";
        yield return hs_get;

        string[] lines = hs_get.text.Split('\n');

        string text = "";
        int i = 1;
        foreach(string s in lines)
        {
            text += i++ + ". " + s + "\n";
        }

        highscoreText.text = text;        
    }


}
