﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour {

    public string pickup;
    public int energyAmount;
    private DistanceTracker distance;

	// Use this for initialization
	void Start () {
        distance = Camera.main.GetComponent<DistanceTracker>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(pickup == "energy")
            {
                collision.gameObject.GetComponentInChildren<PowerBarScript>().modifyEnergy(energyAmount);
            }

            if(pickup == "warpcore")
            {
                collision.gameObject.GetComponentInChildren<PowerBarScript>().modifyWarpCores(1);
            }

            if(pickup == "bonus")
            {
                distance.addBonusPoints();
            }

            Camera.main.GetComponent<AudioSource>().Play();

            Destroy(gameObject);
        }
    }
}
