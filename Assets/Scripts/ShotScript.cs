﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotScript : MonoBehaviour {

    Transform player;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Ship").transform;
	}
	
	// Update is called once per frame
	void Update () {
		if(Vector3.Distance(player.position, transform.position) > 30)
        {
            Destroy(gameObject);
        }
	}
}
