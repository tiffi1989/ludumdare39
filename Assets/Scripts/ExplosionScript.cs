﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("DeactivateLight", .1f);
        Invoke("DestrpyObject", 1f);
        GetComponent<AudioSource>().volume = .6f -  Vector3.Distance(transform.position, GameObject.Find("Ship").transform.position)/30;
	}

    private void DeactivateLight()
    {
        GetComponent<Light>().enabled = false;
    }

    private void DestrpyObject()
    {
        Destroy(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
