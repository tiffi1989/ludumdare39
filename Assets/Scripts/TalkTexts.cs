﻿using UnityEngine;

public class TalkTexts
{
    public string text;
    public AudioClip clip;
    public string name;

   public TalkTexts(string text, string name, AudioClip clip)
    {
        this.text = text;
        this.clip = clip;
        this.name = name;
    }
}