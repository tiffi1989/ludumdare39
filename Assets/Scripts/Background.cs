﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

    public GameObject backgroundPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject.tag);
        if(collision.gameObject.tag == "Player")
        {
            placeNewBackgrounds();
        }
    }

    private void placeNewBackgrounds()
    {
        GameObject[] backgrounds = GameObject.FindGameObjectsWithTag("Background");
        ArrayList positionlist = new ArrayList();
        positionlist.Add(new Vector3(transform.position.x - 19.2f * 4, transform.position.y, 0));
        positionlist.Add(new Vector3(transform.position.x - 19.2f * 4, transform.position.y + 10.8f * 4, 0));
        positionlist.Add(new Vector3(transform.position.x - 19.2f * 4, transform.position.y - 10.8f * 4, 0));
        positionlist.Add(new Vector3(transform.position.x, transform.position.y + 10.8f * 4, 0));
        positionlist.Add(new Vector3(transform.position.x, transform.position.y - 10.8f * 4, 0));
        positionlist.Add(new Vector3(transform.position.x + 19.2f * 4, transform.position.y, 0));
        positionlist.Add(new Vector3(transform.position.x + 19.2f * 4, transform.position.y + 10.8f * 4, 0));
        positionlist.Add(new Vector3(transform.position.x + 19.2f * 4, transform.position.y - 10.8f * 4, 0));

        foreach(Vector3 vector in positionlist)
        {
            bool found = false;
            foreach(GameObject background in backgrounds)
            {
                if (background.transform.position == vector)
                {
                    found = true;
                    break;
                }

            }
            if (found)
                continue;

            Instantiate(backgroundPrefab, vector, Quaternion.identity);
        }
        
    }
}
