﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour {

    Rigidbody2D rig;
    public float speed;
    private float level = 1;
    public int life = 4;
    private int startLife;
    private bool dead;
    public GameObject explosionPrefab;
    private float size;
    private Transform player;
    public GameObject drop;
    public GameObject bonusDrop;

    // Use this for initialization
    void Start () {
        rig = GetComponent<Rigidbody2D>();
        rig.velocity = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f,1f)).normalized * speed * level * Random.Range(1f, 3f);
        rig.AddTorque(Random.Range(7f, 20f));
        size = Random.Range(.5f, 3f) * .3f;
        life = (int)(size * 4) *6 +1;
        startLife = life;
        transform.localScale = new Vector3(size, size, size);
        player = GameObject.Find("Ship").transform;
	}
	
	// Update is called once per frame
	void Update () {
        if(life <= 0 && !dead)
        {
            die();
        }

        if (Vector3.Distance(player.position, transform.position) > 40)
        {
            Destroy(gameObject);
        }

    }

   public void die()
    {
        dead = true;
        GetComponent<CircleCollider2D>().enabled = false;
        rig.velocity = new Vector2();
        rig.angularVelocity = 0;
        GetComponent<SpriteRenderer>().enabled = false;
        GameObject explosion = Instantiate(explosionPrefab, new Vector3(transform.position.x, transform.position.y, -2f), Quaternion.Euler(-90,0,0));
        explosion.transform.localScale = new Vector3(size/2, size/2, size/2);
        Invoke("destroyGO", .5f);
        if(drop != null)
        {
            Instantiate(drop, transform.position, Quaternion.identity).GetComponent<Rigidbody2D>().AddTorque(1f);
        } else
        {
            if (life <= 0 && Random.Range(0,10) == 5)
            {
                Instantiate(bonusDrop, transform.position, Quaternion.identity).GetComponent<Rigidbody2D>().AddTorque(1f);
            }

        }
    }

    private void destroyGO()
    {
        Destroy(gameObject);
    }

    public int getStartLife()
    {
        return startLife;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        Debug.Log(collision.gameObject.tag);
        if(collision.gameObject.tag == "Shot")
        {
            life--;
            life--;
            Destroy(collision.gameObject);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Asteroid" && drop == null)
        {
            if(Random.Range(-4f, 1f) > 0)
                die();
        }
    }
}
