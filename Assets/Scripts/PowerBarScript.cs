﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerBarScript : MonoBehaviour {

    public float energy;
    public int warpCores;
    private SpriteRenderer spriteRenderer;
    public SpriteRenderer warpCore1, warpCore2, warpCore3, warning;
    public Animator warningAni;
    private bool below10, below50, below100;
    private CanvasScript talk;
    private AudioSource source;
    private int bonusPoints;


	// Use this for initialization
	void Start () {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        talk = GameObject.Find("TextBox").GetComponent<CanvasScript>();
        this.source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {

        WarnMessages();

        if(energy <= 20 && energy > 0)
        {
            warningAni.SetBool("isActive", true);
        }
        else
        {
            warningAni.SetBool("isActive", false);
        }

        if(energy <= 0)
        {
            GetComponentInParent<PlayerController>().die();
        }

    }

    private void WarnMessages()
    {
        if(energy >= 100 && below100)
        {
            below100 = false;
            talk.saySomething("energy100");
        }

        if (energy < 100 && !below100)
            below100 = true;

        if(energy < 50 && !below50)
        {
            below50 = true;
            talk.saySomething("energy50");
        }
        if (energy > 50 && below50)
        {
            below50 = false;
            talk.saySomething("energy50");
        }

        if (energy < 20 && !below10)
        {
            below10 = true;
            talk.saySomething("warning");
        }
        if (energy > 20 && below10)
        {
            below10 = false;
            talk.saySomething("warning");
        }
    }

    public void modifyEnergy(float value)
    {
        this.energy += value;
        if (energy > 100)
            energy = 100;
        if (value <= -5)
            source.Play();
        Color oldColor = spriteRenderer.color;
        spriteRenderer.color = Color.Lerp(Color.red, Color.green, energy/100f);
    }

    public void modifyWarpCores(int value)
    {
        if ((value > 0 && warpCores == 3) || (value <= 0 && warpCores == 0))
            return;
        Debug.Log("Warp " +  value + " " +  warpCores);
        if(value == -1)
            talk.saySomething("speed");
        this.warpCores += value;
        switch (warpCores) {
            case 0: disableWarpCores(false, false, false);
                break;
            case 1: disableWarpCores(false, false, true);
                break;
            case 2: disableWarpCores(false, true, true);
                break;
            case 3: disableWarpCores(true, true, true);
                break;
        }
    }

    public int getWarpcores()
    {
        return warpCores;
    }

    private void disableWarpCores(bool one, bool two, bool three)
    {
        warpCore1.enabled = one;
        warpCore2.enabled = two;
        warpCore3.enabled = three;
    }
}
