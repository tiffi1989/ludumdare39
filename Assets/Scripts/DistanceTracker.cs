﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceTracker : MonoBehaviour {

    private float distanceFromStart = 0;
    private float bonusPoints;
    public Transform player;
    public Text text;
    private Highscores high;
    private CanvasScript talk;
    private bool over500, over1000, over2000, over3000, over4000, over5000, over9000;

	// Use this for initialization
	void Start () {
        high = GetComponent<Highscores>();
        talk = GameObject.Find("TextBox").GetComponent<CanvasScript>();
    }

    // Update is called once per frame
    void Update () {
        distanceFromStart = Vector3.Distance(new Vector3(0, 0, 0), player.position);
        text.text = "Distance: " + distanceFromStart.ToString("0") + " + " + bonusPoints.ToString("0");

        if(distanceFromStart > 500 && !over500)
        {
            talk.saySomething("500");
            over500 = true;
        }
        if (distanceFromStart > 1000 && !over1000)
        {
            over1000 = true;
            talk.saySomething("1000");
        }
        if (distanceFromStart > 2000 && !over2000)
        {
            over2000 = true;
            talk.saySomething("2000");
        }
        if (distanceFromStart > 3000 && !over3000)
        {
            over3000 = true;
            talk.saySomething("3000");
        }
        if (distanceFromStart > 4000 && !over4000)
        {
            over4000 = true;
            talk.saySomething("4000");
        }
        if (distanceFromStart > 5000 && !over5000)
        {
            over5000 = true;
            talk.saySomething("5000");
        }
        if (distanceFromStart > 9000 && !over9000)
        {
            over9000 = true;
            talk.saySomething("9000");
        }

        if (Input.GetKeyDown("d"))
        {
           // high.addHighscore();
        }
    }

    public void addBonusPoints()
    {
        bonusPoints += distanceFromStart / 4;
    }


    public float getDistanceFromStart()
    {
        return distanceFromStart;
    }


    public float getFinalPoints()
    {
        return distanceFromStart + bonusPoints;
    }
}
