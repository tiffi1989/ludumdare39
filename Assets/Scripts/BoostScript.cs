﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostScript : MonoBehaviour {

    private SpriteRenderer renderer;
    private Light light;

	// Use this for initialization
	void Start () {
        renderer = GetComponent<SpriteRenderer>();
        light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        light.enabled = renderer.enabled;
	}
}
