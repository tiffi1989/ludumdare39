﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour {

    public GameObject bulletPrefab;
    public float shotspeed;
    public PowerBarScript powerBar;
    public PlayerController player;

	// Use this for initialization
	void Start () {
        player = GetComponentInParent<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!player.isGameStarted())
        {
            return;
        }

        Vector3 mouse = Input.mousePosition;
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(transform.position);
        Vector2 offset = new Vector2(mouse.x - screenPoint.x, mouse.y - screenPoint.y);
        float angle = Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, angle);

        if(Input.GetButtonDown("Fire1"))
        {
            if(powerBar.energy > 2)
            {
                GameObject bullet = Instantiate(bulletPrefab, transform.position, transform.rotation);
                bullet.GetComponent<Rigidbody2D>().velocity = Turning().normalized * shotspeed;
                powerBar.modifyEnergy(-.1f);
            }
        }
    }

    Vector2 Turning()
    {
        Vector3 shotPixelposition = Camera.main.WorldToScreenPoint(transform.position);
        return new Vector2(
            Input.mousePosition.x - shotPixelposition.x,
            Input.mousePosition.y - shotPixelposition.y);
    }
}
