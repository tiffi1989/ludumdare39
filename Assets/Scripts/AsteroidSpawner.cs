﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour {

    public GameObject asterPrefab, uranPrefab, warpPrefab, enemyPrefab;
    public Rigidbody2D playerRig;
    private DistanceTracker distance;
    public float spawnTime;
    private float timer;
    public int pickUpRate;
    private int pickUpCounter;
    private float enemySpawnTimer;
    public float enemySpawnTime;

    // Use this for initialization
    void Start () {
        distance = GetComponent<DistanceTracker>();
    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        float div = (distance.getDistanceFromStart() / 3) < 300 ? (distance.getDistanceFromStart() / 3) : 300;

        if (distance.getDistanceFromStart() > 700)
        {
            div = 100;
            enemySpawnTimer += Time.deltaTime;
            if(enemySpawnTimer > enemySpawnTime / (distance.getDistanceFromStart() / 600))
            {
                spawnEnemy();
                enemySpawnTimer = 0;
    
            }
        }

        if (timer >= spawnTime / div)
        {
            pickUpCounter++;
            if(pickUpCounter >= pickUpRate)
            {
                spawnPickup();
                pickUpCounter = 0;
            }else
            {
                spawnAsteroid();
            }


            timer = 0;
        }
     }

    private void spawnEnemy()
    {
        Instantiate(enemyPrefab, new Vector3(transform.position.x + 30, transform.position.y + Random.Range(-20f, 20f), 0), Quaternion.identity);
        Instantiate(enemyPrefab, new Vector3(transform.position.x - 30, transform.position.y + Random.Range(-20f, 20f), 0), Quaternion.identity);
        Instantiate(enemyPrefab, new Vector3(transform.position.x + Random.Range(-20f, 20f), transform.position.y + 20, 0), Quaternion.identity);
        Instantiate(enemyPrefab, new Vector3(transform.position.x + Random.Range(-20f, 20f), transform.position.y - 20, 0), Quaternion.identity);
    }

    private void spawnAsteroid()
    {
        Instantiate(asterPrefab, new Vector3(transform.position.x + 30, transform.position.y + Random.Range(-20f, 20f), 0), Quaternion.identity);
        Instantiate(asterPrefab, new Vector3(transform.position.x - 30, transform.position.y + Random.Range(-20f, 20f), 0), Quaternion.identity);
        Instantiate(asterPrefab, new Vector3(transform.position.x + Random.Range(-20f, 20f), transform.position.y + 20, 0), Quaternion.identity);
        Instantiate(asterPrefab, new Vector3(transform.position.x + Random.Range(-20f, 20f), transform.position.y -20, 0), Quaternion.identity);
    }

    private void spawnPickup()
    {
        GameObject pickup = null;
        if(Random.Range(-1f,2f) > 0)
        {
            pickup = uranPrefab;
        }else
        {
            pickup = warpPrefab;
        }
        Instantiate(pickup, new Vector3(transform.position.x + 30, transform.position.y + Random.Range(-20f, 20f), 0), Quaternion.identity);
        Instantiate(pickup, new Vector3(transform.position.x - 30, transform.position.y + Random.Range(-20f, 20f), 0), Quaternion.identity);
        Instantiate(pickup, new Vector3(transform.position.x + Random.Range(-20f, 20f), transform.position.y + 20, 0), Quaternion.identity);
        Instantiate(pickup, new Vector3(transform.position.x + Random.Range(-20f, 20f), transform.position.y - 20, 0), Quaternion.identity);
    }
}
